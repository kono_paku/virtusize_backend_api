from django.db import models
from django.core.validators import MinLengthValidator
from backend_api.shared.constant import Constant

"""Provides model for user information
:param models.Model: Inheritance for the Model class of Django Framework
"""
class UserInfo(models.Model):
  id = models.AutoField(primary_key = True)
  name = models.CharField(max_length = Constant.NAME_MAX_LEGNTH_INT)
  email = models.EmailField(max_length = Constant.EMAIL_MAX_LENGTH_INT)
  password = models.CharField(
    blank = True,
    max_length = Constant.PASSWORD_MAX_LENGTH_INT
  )

  """Return the object name as id value
  :param self: Self variable to register the member function in the instance.
  :return str(self.id): id string value of user information.
  """
  def __str__(self):
    return str(self.id)
