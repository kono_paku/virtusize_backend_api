from rest_framework import serializers
from backend_api.models import UserInfo
from backend_api.shared.constant import Constant

"""Serializer for request and response data
"""
class UserInfoSerializer(serializers.ModelSerializer):
  """Meta information used to serialize the data 
  """
  class Meta:
    model = UserInfo
    fields = (
        Constant.ID_STRING,
        Constant.NAME_STRING,
        Constant.EMAIL_STRING,
        Constant.PASSWORD_STRING
    )
