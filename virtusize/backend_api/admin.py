from django.contrib import admin
from backend_api.models import UserInfo

# Register in order to use the page of admin
admin.site.register(UserInfo)
