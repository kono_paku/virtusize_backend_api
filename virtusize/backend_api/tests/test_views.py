# import from Django framework
from django.test import TestCase
from django.core.urlresolvers import reverse

# import from Django rest framework
from rest_framework.test import APIClient
from rest_framework.test import APITestCase
from rest_framework import status

# import from custom source code
from backend_api.shared.constant import Constant

"""Test HTTP request each methods for API
:param APITestCase: Inheritance for the Test class of Django Rest Framework
"""
class BackendAPITests(APITestCase):
  
  """Set up an input data prerequisite condition before testing each methods
  :param self: Self variable to register the member function in the instance.
  """
  def setUp(self):
    self.client = APIClient()
    response = self.client.post(
      Constant.TEST_URL_STRING,
      data = Constant.TEST_SET_UP_JSON_STRING,
      content_type = Constant.JSON_CONTENT_TYPE_STRING
    )

  """Test a basic feature of get method by retrieving the response
  :param self: Self variable to register the member function in the instance.
  """
  def test_get_method(self):
    response = self.client.get(
      Constant.TEST_URL_STRING
      + Constant.TEST_ID_PARAMETER_STRING
    )
    self.assertEqual(
      str(response.data),
      Constant.TEST_GET_EXPECTED_RESULT_STRING
    )

  """Test a basic feature of post method by retrieving the response
  :param self: Self variable to register the member function in the instance.
  """
  def test_post_method(self):
    response = self.client.post(
      Constant.TEST_URL_STRING,
      data = Constant.TEST_POST_JSON_STRING,
      content_type = Constant.JSON_CONTENT_TYPE_STRING
    )
    self.assertEqual(
      str(response.data),
      Constant.TEST_POST_EXPECTED_RESULT_STRING
    )

  """Test a basic feature of put method by retrieving the response
  :param self: Self variable to register the member function in the instance.
  """
  def test_put_method(self):
    response = self.client.put(
      Constant.TEST_URL_STRING,
      data = Constant.TEST_PUT_JSON_STRING,
      content_type = Constant.JSON_CONTENT_TYPE_STRING
    )
    self.assertEqual(
      str(response.data),
      Constant.TEST_PUT_EXPECTED_RESULT_STRING
    )

  """Test a basic feature of delete method by retrieving the response
  :param self: Self variable to register the member function in the instance.
  """
  def test_delete_method(self):
    response = self.client.delete(
      Constant.TEST_URL_STRING
      + Constant.TEST_ID_PARAMETER_STRING
    )
    self.assertEqual(
      str(response.data),
      Constant.TEST_DELETE_EXPECTED_RESULT_STRING
    )
