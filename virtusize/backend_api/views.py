# impot from python
import json
import hashlib

# import from Django framework
from django.shortcuts import render
from django.core.exceptions import ValidationError

# import from Django rest framework
from rest_framework.views import APIView

# import from custom source code
from backend_api.models import UserInfo
from backend_api.serializers import UserInfoSerializer
from backend_api.shared.constant import Constant
from backend_api.shared.response_handler import ResponseHandler


"""Provide response based on request in json format.
:param APIView: Inheritance for the APIView class of Django rest framework.
"""
class User(APIView):
  """Deal with get method request of API.
  :param self: Self variable to register the member function in the instance.
  :param request: Variable contains HTTP information of request.
  :param id: Variable from id parameter of request to get user information
  :return Response: Variable including user or error information in json format
  """
  def get(self, request, id = Constant.EMPTY_STRING):
    # Set initialize value for a returinig dictionary
    return_dict = {}

    # Create ResponseHandler consisting of response based on request.
    response_handler = ResponseHandler()

    # Return response of error for bad request if id is emtpy or null.
    if not id:
      return_dict[Constant.RETURN_VALUE_STRING] = response_handler.get_http_bad_request_response(
        {
          Constant.RESULT_DETAIL_STRING: Constant.ID_NOT_BE_EMPTY_ERROR_MESSAGE_STRING
        }
      )
      return return_dict[Constant.RETURN_VALUE_STRING]

    # Search for user information by id from request parameters
    user_info = UserInfo.objects.filter(
      id=id
    ).first()

    # Return the successful reponse if there is matching information by id.
    if user_info:
      return_dict[Constant.RETURN_VALUE_STRING] = response_handler.get_http_ok_response(
        {
          Constant.NAME_STRING: user_info.name,
          Constant.EMAIL_STRING: user_info.email
        }
      )
    # Return error response which inform user information has not been found.
    else:
      return_dict[Constant.RETURN_VALUE_STRING] = response_handler.get_http_ok_response(
        {
          Constant.RESULT_DETAIL_STRING: Constant.NOT_FOUND_USER_INFO_ERROR_MESSAGE_STRING
        }
      )
    return return_dict[Constant.RETURN_VALUE_STRING]

  """Deal with post method request of API.
  :param self: Self variable to register the member function in the instance.
  :param request: Variable contains HTTP information of request.
  :return Response: Variable including user or error information in json format
  """
  def post(self, request):
    # Set initialize value for a returinig dictionary
    return_dict = {}

    # Create ResponseHandler consisting of response based on request.
    response_handler = ResponseHandler()

    # Check the content type of request.
    return_dict = response_handler.check_content_type(request)
    
    # Return the reponse if return code is not successful
    if return_dict[Constant.RETURN_CODE_STRING] != Constant.SUCCESS_VALUE_INT:
      return return_dict[Constant.RETURN_VALUE_STRING]

    # Convert json byte in to dictionary.
    json_request = json.loads(request.body)

    # Validate the password value.
    return_dict = response_handler.validate_password(json_request.get(Constant.PASSWORD_STRING))

    # Return the reponse if return code is not successful
    if return_dict[Constant.RETURN_CODE_STRING] != Constant.SUCCESS_VALUE_INT:
      return return_dict[Constant.RETURN_VALUE_STRING]

    # Input each json value into UserInfoSerializer for validation.
    # And convert password into hash value by using sha256 algorithm.
    user_info_serializer = UserInfoSerializer(
      data={
        Constant.NAME_STRING: json_request.get(Constant.NAME_STRING),
        Constant.EMAIL_STRING: json_request.get(Constant.EMAIL_STRING),
        Constant.PASSWORD_STRING: hashlib
        .sha256(
          json_request
          .get(Constant.PASSWORD_STRING)
          .encode(Constant.UTF8_STRING)
        )
        .hexdigest(),
      }
    )

    # Validate the input values by using serializer.
    return_dict = response_handler.validate_serializer(user_info_serializer)

    # Return the reponse if return code is not successful
    if return_dict[Constant.RETURN_CODE_STRING] != Constant.SUCCESS_VALUE_INT:
      return return_dict[Constant.RETURN_VALUE_STRING]

    user_info = UserInfo.objects.filter(
      name=json_request.get(Constant.NAME_STRING),
      email=json_request.get(Constant.EMAIL_STRING)
    ).first()

    # Save user information into Database in case there is no any the same one.
    if not user_info:
      user_info_serializer.save()
      return_dict[Constant.RETURN_VALUE_STRING] = response_handler.get_http_ok_response(
        {
          Constant.ID_STRING: user_info_serializer.data[Constant.ID_STRING],
          Constant.NAME_STRING: user_info_serializer.data[Constant.NAME_STRING],
          Constant.EMAIL_STRING: user_info_serializer.data[Constant.EMAIL_STRING]
        }
      )
    # Otherwise, return the response of error which informs that the same information exists.
    else:
      return_dict[Constant.RETURN_VALUE_STRING] = response_handler.get_http_ok_response(
        {
          Constant.RESULT_DETAIL_STRING: Constant.DULPLICATED_USER_INFO_ERROR_MESSAGE_STRING
        }
      )

    return return_dict[Constant.RETURN_VALUE_STRING]

  """Deal with put method request of API.
  :param self: Self variable to register the member function in the instance.
  :param request: Variable contains HTTP information of request.
  :return Response: Variable including user or error information in json format
  """
  def put(self, request):
    # Set initialize value for a returinig dictionary
    return_dict = {}

    # Create ResponseHandler consisting of response based on request.
    response_handler = ResponseHandler()

    # Check the content type of request.
    return_dict = response_handler.check_content_type(request)
    
    # Return the reponse if return code is not successful
    if return_dict[Constant.RETURN_CODE_STRING] != Constant.SUCCESS_VALUE_INT:
      return return_dict[Constant.RETURN_VALUE_STRING]

    # Convert json byte in to dictionary.
    json_request = json.loads(request.body)

    # Input each json value into UserInfoSerializer for validation.
    # And convert password into hash value by using sha256 algorithm.
    user_info_serializer = UserInfoSerializer(
      data = {
        Constant.ID_STRING: json_request.get(Constant.ID_STRING),
        Constant.NAME_STRING: json_request.get(Constant.NAME_STRING),
        Constant.EMAIL_STRING: json_request.get(Constant.EMAIL_STRING),
        Constant.PASSWORD_STRING: hashlib
        .sha256(
          json_request
          .get(Constant.PASSWORD_STRING)
          .encode(Constant.UTF8_STRING)
        )
        .hexdigest()
      }
    )

    # Validate the input values by using serializer.
    return_dict = response_handler.validate_serializer(user_info_serializer)

    # Return the reponse if return code is not successful
    if return_dict[Constant.RETURN_CODE_STRING] != Constant.SUCCESS_VALUE_INT:
      return return_dict[Constant.RETURN_VALUE_STRING]

    # Check whether there is the same user information with the request
    existed_user_info = UserInfo.objects.filter(
        name=json_request.get(Constant.NAME_STRING),
        email=json_request.get(Constant.EMAIL_STRING)
    ).first()

    # If there is the one, return the response of error which informs that the same information exists.
    if existed_user_info:
      return_dict[Constant.RETURN_VALUE_STRING] = response_handler.get_http_ok_response(
          {
              Constant.RESULT_DETAIL_STRING: Constant.DULPLICATED_USER_INFO_ERROR_MESSAGE_STRING
          }
      )
      return return_dict[Constant.RETURN_VALUE_STRING]

    # Get user information to update from model
    user_info = UserInfo.objects.filter(
      id = json_request.get(Constant.ID_STRING),
    ).first()

    # Save user information into Database in case there is user information by target id.
    if user_info:
      user_info.name = user_info_serializer.data[Constant.NAME_STRING]
      user_info.email = user_info_serializer.data[Constant.EMAIL_STRING]
      user_info.password = user_info_serializer.data[Constant.PASSWORD_STRING]

      user_info.save()
      return_dict[Constant.RETURN_VALUE_STRING] = response_handler.get_http_ok_response(
        {
          Constant.ID_STRING: user_info.id,
          Constant.NAME_STRING: user_info.name,
          Constant.EMAIL_STRING: user_info.email
        }
      )
    # Otherwise, return not found user error response
    else:      
      return_dict[Constant.RETURN_VALUE_STRING] = response_handler.get_http_ok_response(
        {
          Constant.RESULT_DETAIL_STRING: Constant.NOT_FOUND_USER_INFO_ERROR_MESSAGE_STRING
        }
      )
    return return_dict[Constant.RETURN_VALUE_STRING]

  """Deal with delete method request of API.
  :param self: Self variable to register the member function in the instance.
  :param request: Variable contains HTTP information of request.
  :param id: Variable from id parameter of request to delete user information
  :return Response: Variable including user or error information in json format
  """
  def delete(self, request, id=Constant.EMPTY_STRING):
    # Set initialize value for a returinig dictionary
    return_dict = {}

    # Create ResponseHandler consisting of response based on request.
    response_handler = ResponseHandler()

    # Return response of error for bad request if id is emtpy or null.
    if not id:
      return_dict[Constant.RETURN_VALUE_STRING] = response_handler.get_http_bad_request_response(
        {
          Constant.RESULT_DETAIL_STRING: Constant.ID_NOT_BE_EMPTY_ERROR_MESSAGE_STRING
        }
      )
      return return_dict[Constant.RETURN_VALUE_STRING]

    # Search for user information by id from request parameters
    user_info = UserInfo.objects.filter(
      id = id
    ).first()

    # Return the successful reponse if there is matching information by id.
    if user_info:
      user_info.delete()
      return_dict[Constant.RETURN_VALUE_STRING] = response_handler.get_http_ok_response(
        {
          Constant.ID_STRING: int(id)
        }
      )
    # Return error response which inform user information has not been found.
    else:
      return_dict[Constant.RETURN_VALUE_STRING] = response_handler.get_http_ok_response(
        {
          Constant.RESULT_DETAIL_STRING: Constant.NOT_FOUND_USER_INFO_ERROR_MESSAGE_STRING
        }
      )
    return return_dict[Constant.RETURN_VALUE_STRING]
