"""Collect contant variables
"""
class Constant:

  # String constant values
  ID_STRING = "id"
  NAME_STRING = "name"
  EMAIL_STRING = "email"
  PASSWORD_STRING = "password"
  JSON_CONTENT_TYPE_STRING = "application/json"
  UTF8_STRING = "utf8"
  RETURN_CODE_STRING = "return_code"
  RETURN_VALUE_STRING = "return_value"
  RESULT_CODE_STRING = "result_code"
  RESULT_MESSAGE_STRING = "result_message"
  RESULT_VALUE_STRING = "result_value"
  RESULT_DETAIL_STRING = "result_detail"
  EMPTY_STRING = ""
  ERROR_LOG_STRING = "<< ERROR >> "
  GET_METHOD_STRING = "GET"
  POST_METHOD_STRING = "POST"
  PUT_METHOD_STRING = "PUT"
  DELETE_METHOD_STRING = "DELETE"
  PATCH_METHOD_STRING = "PATCH"
  HEAD_METHOD_STRING = "HEAD"
  CONNECT_METHOD_STRING = "CONNECT"
  OPTIONS_METHOD_STRING = "OPTIONS"
  TRACE_METHOD_STRING = "TRACE"
  HTTP_OK_STATUS_MESSAGE_STRING = "OK"
  HTTP_BAD_REQUEST_STATUS_MESSAGE_STRING = "Bad Request"
  HTTP_METHOD_NOT_ALLOWED_STATUS_MESSAGE_STRING = "Method Not Allowed"
  HTTP_INTERNAL_SERVER_ERROR_STATUS_MESSAGE_STRING = "Internal Server Error"
  HTTP_CONTENT_TYPE_ERROR_MESSAGE_STRING = "Content-Type should be application/json."
  HTTP_SUPPORTED_METHOD_ERROR_MESSAGE_STRING = "GET, POST, PUT and DELETE method are only supported."
  HTTP_CURL_REQUEST_PROCESS_ERROR_MESSAGE_STRING = "Curl request has failed."
  ID_NOT_BE_EMPTY_ERROR_MESSAGE_STRING = "ID should not be null or empty."
  PASSWORD_NOT_BE_EMPTY_ERROR_MESSAGE_STRING = "Password should not be null or empty."
  PASSWORD_UNDER_MIN_LENGTH_ERROR_MESSAGE_STRING = "The length of password should be at least 8 and under 128."
  DULPLICATED_USER_INFO_ERROR_MESSAGE_STRING = "The same user information already exists!"
  NOT_FOUND_USER_INFO_ERROR_MESSAGE_STRING = "The user information has not been found by id."
  TEST_URL_STRING = "/user/"
  TEST_ID_PARAMETER_STRING = "1/"
  TEST_SET_UP_JSON_STRING = '{"name": "Emily Chung","email": "emily.chung@gmaill.com","password": "13241324"}'
  TEST_GET_EXPECTED_RESULT_STRING = "{'result_code': 200, 'result_message': 'OK', 'result_value': {'name': 'Emily Chung', 'email': 'emily.chung@gmaill.com'}}"
  TEST_POST_JSON_STRING = '{"name": "James Weaver","email": "james.weaver@gmaill.com","password": "1111222233334444"}'
  TEST_POST_EXPECTED_RESULT_STRING = "{'result_code': 200, 'result_message': 'OK', 'result_value': {'id': 2, 'name': 'James Weaver', 'email': 'james.weaver@gmaill.com'}}"
  TEST_PUT_JSON_STRING = '{"id": "1", "name": "Emily Fisher","email": "emily.fisher@gmaill.com","password": "132412323"}'
  TEST_PUT_EXPECTED_RESULT_STRING = "{'result_code': 200, 'result_message': 'OK', 'result_value': {'id': 1, 'name': 'Emily Fisher', 'email': 'emily.fisher@gmaill.com'}}"
  TEST_DELETE_EXPECTED_RESULT_STRING = "{'result_code': 200, 'result_message': 'OK', 'result_value': {'id': 1}}"

  # Integer constant values
  NUMBER_ZERO_INT = 0
  NUMBER_ONE_INT = 1
  NUMBER_TEN_INT = 10
  WORD_MAX_LENGTH_INT = 20
  TIME_OUT_INT = 10
  SUCCESS_VALUE_INT = NUMBER_ZERO_INT
  FAILURE_VALUE_INT = NUMBER_ONE_INT
  URL_EMPTY_OR_NULL_INT = NUMBER_ZERO_INT
  NAME_MAX_LEGNTH_INT = 35
  EMAIL_MAX_LENGTH_INT = 255
  PASSWORD_MAX_LENGTH_INT = 128
  PASSWORD_MIN_LENGTH_INT = 8
