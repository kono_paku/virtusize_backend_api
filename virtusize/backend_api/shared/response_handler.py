# impot from python
import logging

# import from Django rest framework
from rest_framework import status
from rest_framework.response import Response

# import from custom source code
from backend_api.shared.constant import Constant

# Set up a logger for checking the error message.
logger = logging.getLogger(__name__)

"""Response Handler for validating request and consisting of response
"""
class ResponseHandler():
  """Check content type from the request whether application/json is
  :param self: Self variable to register the member function in the instance.
  :param request: Variable containing HTTP information of request.
  :return return_dict: Dictionary variable including return code and return value.
  """
  def check_content_type(self, request):
    # Initialize for return value and response.
    return_dict = {
      Constant.RETURN_CODE_STRING : Constant.SUCCESS_VALUE_INT
    }
    response_dict = {}

    # Check content_type whether application/json is.
    if request.content_type != Constant.JSON_CONTENT_TYPE_STRING:
      logger.error(Constant.ERROR_LOG_STRING + Constant.HTTP_CONTENT_TYPE_ERROR_MESSAGE_STRING)

      # Set faliure code and response into return dict
      return_dict[Constant.RETURN_CODE_STRING] = Constant.FAILURE_VALUE_INT
      return_dict[Constant.RETURN_VALUE_STRING] = self.get_http_bad_request_response(
        {
          Constant.RESULT_DETAIL_STRING: Constant.HTTP_CONTENT_TYPE_ERROR_MESSAGE_STRING
        }
      );
    
    return return_dict

  """Validate the password value from request before hashed whether it contains empty or the proper length value.
  :param self: Self variable to register the member function in the instance.
  :param password: Variable for password value from requst before hashed by sha256.
  :return return_dict: Dictionary variable including return code and return value.
  """
  def validate_password(self, password):
    # Initialize for return value and response.
    return_dict = {
        Constant.RETURN_CODE_STRING: Constant.SUCCESS_VALUE_INT
    }
    response_dict = {}

    # Check password is null or empty
    if not password:
      logger.error(Constant.ERROR_LOG_STRING + Constant.PASSWORD_NOT_BE_EMPTY_ERROR_MESSAGE_STRING)

      # Set faliure code and response into return dict
      return_dict[Constant.RETURN_CODE_STRING] = Constant.FAILURE_VALUE_INT
      return_dict[Constant.RETURN_VALUE_STRING] = self.get_http_bad_request_response(
        {
          Constant.RESULT_DETAIL_STRING: Constant.PASSWORD_NOT_BE_EMPTY_ERROR_MESSAGE_STRING
        }
      )
      return return_dict
    
    # Calculate the length of password
    length_of_password = len(password)

    # If it exists, check the length of it is over 8 and under 128.
    if (length_of_password < Constant.PASSWORD_MIN_LENGTH_INT
      or length_of_password > Constant.PASSWORD_MAX_LENGTH_INT):
      logger.error(Constant.ERROR_LOG_STRING + Constant.PASSWORD_UNDER_MIN_LENGTH_ERROR_MESSAGE_STRING)

      # Set faliure code and response into return dict
      return_dict[Constant.RETURN_CODE_STRING] = Constant.FAILURE_VALUE_INT
      return_dict[Constant.RETURN_VALUE_STRING] = self.get_http_bad_request_response(
        {
          Constant.RESULT_DETAIL_STRING: Constant.PASSWORD_UNDER_MIN_LENGTH_ERROR_MESSAGE_STRING
        }
      )

    return return_dict

  """Validate values from reqeust on each fields by using is_valid() built-in function.
  :param self: Self variable to register the member function in the instance.
  :param serializer: Serializer contains values from request.
  :return return_dict: Dictionary variable including return code and return value.
  """
  def validate_serializer(self, serializer):
    # Initialize for return value and response.
    return_dict = {
        Constant.RETURN_CODE_STRING: Constant.SUCCESS_VALUE_INT
    }
    response_dict = {}

    # Check whether the request parameters are valid.
    if not serializer.is_valid():
      logger.error(Constant.ERROR_LOG_STRING + serializer.errors)

      # Set faliure code and response into return dict
      return_dict[Constant.RETURN_CODE_STRING] = Constant.FAILURE_VALUE_INT
      return_dict[Constant.RETURN_VALUE_STRING] = self.get_http_bad_request_response(
        {
          Constant.RESULT_DETAIL_STRING: serializer.errors
        }
      )
    
    return return_dict

  """Compose response including ok status code, status message, user information and the detail of error.
  :param self: Self variable to register the member function in the instance.
  :param result_dict: Dictionary variable containing user information or the detail of error.
  :return Response: Response variable including status code, user information or error message.
  """
  def get_http_ok_response(self, result_dict):
    # Initialize for return value and response.
    response_dict = {}
    status_code = status.HTTP_200_OK

    # Compose response depending on the type.
    response_dict.update(
      {
        Constant.RESULT_CODE_STRING: status_code,
        Constant.RESULT_MESSAGE_STRING: Constant.HTTP_OK_STATUS_MESSAGE_STRING,
        Constant.RESULT_VALUE_STRING: result_dict
      }
    )
    
    return Response(
      response_dict,
      status=status_code
    )

  """Compose response including bad reqeust status code, message as well as the detail of error.
  :param self: Self variable to register the member function in the instance.
  :param result_dict: Dictionary variable containing the detail of error.
  :return Response: Response variable including status code and error message.
  """
  def get_http_bad_request_response(self, result_dict):
    # Initialize for return value and response.
    response_dict = {}
    status_code = status.HTTP_400_BAD_REQUEST

    # Compose response depending on the type.
    response_dict.update(
      {
        Constant.RESULT_CODE_STRING: status_code,
        Constant.RESULT_MESSAGE_STRING: Constant.HTTP_BAD_REQUEST_STATUS_MESSAGE_STRING,
        Constant.RESULT_VALUE_STRING: result_dict
      }
    )

    return Response(
      response_dict,
      status=status_code
    )
