#Backend API
This endpoint explains Backend API.
  
  
##HTTP GET Request
----
`GET http://ec2-52-15-177-144.us-east-2.compute.amazonaws.com:8000/user/1`
    

###Request Format

**Parameters :**

Name          | Type           | Description                    
------------- | -------------- |------------------------------ 
`id`          | int            | **Required** Target user id to get user information.   


###Response Format
   
**Success Content : **

Name                | Type           | Description                    
------------------- | -------------- |------------------------------ 
`result_code`       | integer        | HTTP status code
`result_message`    | string         | HTTP status message
`result_value`      | json           | User information


**Example:**

    {
        "result_code": 200,
        "result_message": "OK",
        "result_value": {
          "name": "Gunho Park",
          "email": "kono.paku@gmail.com"
        }
    }

**Error Content : **

Name                | Type           | Description                    
------------------- | -------------- |------------------------------ 
`result_code`       | integer        | HTTP status code
`result_message`    | string         | HTTP status message
`result_value`      | json           | Result message for error in detail

**Example:**
    
    {
        "result_code": 200,
        "result_message": "OK",
        "result_value": {
          "result_detail": "The user information has not been found by id."
        }
    }





##HTTP POST Request
----
`POST http://ec2-52-15-177-144.us-east-2.compute.amazonaws.com:8000/user`
    

###Request Format
**Content-Type :**

`application/json`

**Parameters :**

Name          | Type           | Description                    
------------- | -------------- |------------------------------ 
`name`        | string         | **Required** Target name to input into database. The maximum length is 35.
`email`       | string         | **Required** The input email to input into database. The maximum length is 255.
`password`    | string         | **Required** The input password to input into database. The length is at least 8 and under 128.     

**Example :**

    {
      "name" : "Michael Weaver",
      "email": "michael.weaver@gmail.com",
      "password": "13241324"
    }

###Response Format
**Content-Type : **

`application/json`
   
**Success Content : **

Name                | Type           | Description                    
------------------- | -------------- |------------------------------ 
`result_code`       | integer        | HTTP status code
`result_message`    | string         | HTTP status message
`result_value`      | json           | User information


**Example:**

    {
        "result_code": 200,
        "result_message": "OK",
        "result_value": {
          "id": 18,
          "name": "Michael Weaver",
          "email": "michael.weaver@gmail.com"
        }
    }

**Error Content : **

Name                | Type           | Description                    
------------------- | -------------- |------------------------------ 
`result_code`       | integer        | HTTP status code
`result_message`    | string         | HTTP status message
`result_value`      | json           | Error message in detail

**Example:**
    
    {
        "result_code": 400,
        "result_message": "Bad Request",
        "result_value": {
          "result_detail": "The length of password should be at least 8 and under 128."
        }
    }



##HTTP PUT Request
----
`PUT http://ec2-52-15-177-144.us-east-2.compute.amazonaws.com:8000/user`
    

###Request Format
**Content-Type :**

`application/json`

**Parameters :**

Name          | Type           | Description                    
------------- | -------------- |------------------------------ 
`id`          | int or string  | **Required** Target id to update into database.
`name`        | string         | **Required** Target name to update into database. The maximum length is 35.
`email`       | string         | **Required** The input email to update into database. The maximum length is 255.
`password`    | string         | **Required** The input password to update into database. The length is at least 8 and under 128.     

**Example :**

    {
      "id" : 14,
      "name" : "Erwann Park",
      "email": "erwann.park@gmail.com",
      "password": "13241324"
    }

###Response Format
**Content-Type : **

`application/json`
   
**Success Content : **

Name                | Type           | Description                    
------------------- | -------------- |------------------------------ 
`result_code`       | integer        | HTTP status code
`result_message`    | string         | HTTP status message
`result_value`      | json           | User information


**Example:**

    {
        "result_code": 200,
        "result_message": "OK",
        "result_value": {
          "id": 14,
          "name": "Erwann Park",
          "email": "erwann.park@gmail.com"
        }
    }

**Error Content : **

Name                | Type           | Description                    
------------------- | -------------- |------------------------------ 
`result_code`       | integer        | HTTP status code
`result_message`    | string         | HTTP status message
`result_value`      | json           | Error message in detail

**Example:**
    
    {
        "result_code": 400,
        "result_message": "Bad Request",
        "result_value": {
          "result_detail": {
            "email": [
              "This field may not be null."
            ]
          }
        }
    }
    

##HTTP DELETE Request
----
`DELETE http://ec2-52-15-177-144.us-east-2.compute.amazonaws.com:8000/user/14`
    

###Request Format

**Parameters :**

Name          | Type           | Description                    
------------- | -------------- |------------------------------ 
`id`          | int            | **Required** Target user id to get user information.   


###Response Format
   
**Success Content : **

Name                | Type           | Description                    
------------------- | -------------- |------------------------------ 
`result_code`       | integer        | HTTP status code
`result_message`    | string         | HTTP status message
`result_value`      | json           | Deleted ID information 


**Example:**

    {
        "result_code": 200,
        "result_message": "OK",
        "result_value": {
          "id": "Gunho Park"
        }
    }

**Error Content : **

Name                | Type           | Description                    
------------------- | -------------- |------------------------------ 
`result_code`       | integer        | HTTP status code
`result_message`    | string         | HTTP status message
`result_value`      | json           | Result message for error in detail

**Example:**
    
    {
        "result_code": 200,
        "result_message": "OK",
        "result_value": {
          "result_detail": "The user information has not been found by id."
        }
    }
