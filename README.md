# Backend API

This page describes about how to run the source codes for Backend API.

## Getting Started

These instructions will get you an how to run on your local machine for development and testing purposes. 

### Prerequisites

* [Python 3.6.2](https://www.python.org/downloads/) - For using Python3 environment
* [Django Framework 1.11.13](https://www.djangoproject.com/download/) - For providing web server
* [djangorestframework 3.8.2](http://www.django-rest-framework.org/#installation) - For consisting of CRUD REST API

### Deployment

Download the source codes into local.

```
$ git clone git@bitbucket.org:kono_paku/virtusize_backend_api.git
```

Move to directory in order to execute manage.py.

```
$ cd virtusize_backend_api/virtusize
```

Run source codes by using command below.

```
$ python manage.py runserver 0.0.0.0:8000
```

### Running the tests

Check the result of test by executing command below. 

```
$ python manage.py test backend_api/tests
```
